const express = require('express');
const app = express();

app.get('/', (req, res) => {
  res.send('Desaio DevOps!');
});

const server = app.listen(3030, () => {
  console.log(`Express running → PORT ${server.address().port}`);
});